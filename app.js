const express = require('express'); 
const path = require('path');
const app = express();
const bodyParser = require('body-parser');
var methodOverride = require('method-override');
const { mongoose } = require('./database/db');
/* import */

const routes = require('./routes/index')

/* settings */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')
 
/* midlewares */
app.use((req,res,next)=>{
    console.log(`${req.url}- ${req.method}`);
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(methodOverride('_method'));

/* routes */
app.use(routes);

/* static files */
app.use(express.static(path.join(__dirname, 'public') ))

/* start */
app.listen(app.get('port'), ()=> {
    console.log('Servidor escuchando en el puerto -->', app.get('port'), '\n  🚀💫☁🔥🚀🔥🪐🚀☄️')
});