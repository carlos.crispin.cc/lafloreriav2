module.exports =[
    {
        id:0,
        nombre: 'tuna',
        precio: 50,
        descripcion: 'aut non nostrumaut non nostrumaut non nostrum',
        foto: 'https://image.freepik.com/foto-gratis/cactus-espinoso-verde-sobre-fondo-rosa_23-2147842416.jpg',
        categoria: 'sombra',
        stock: 15,
        fecha: "Jue, 1 Octubre 2020 04:53:09 PM",
    },
    {
        id: 1,
        nombre: 'Nopal',
        precio: 50,
        descripcion: 'aut non nostrumaut non nostrumaut non nostrum aut non nostrumaut non nostrumaut non nostrum aut non nostrumaut non nostrumaut non nostrum',
        foto: 'https://image.freepik.com/foto-gratis/planta-oficina_23-2148003008.jpg',
        categoria: 'sombra',
        stock: 15,
        fecha: "Jue, 1 Octubre 2020 04:53:09 PM",
    },
    {
        id: 2,
        nombre: 'Suculenta 3',
        precio: 40,
        descripcion: 'aut non nostrumaut non nostrumaut non nostrum',
        foto: 'https://image.freepik.com/foto-gratis/disparo-aislado-selectivo-vertical-planta-cactus-verde-maceta-blanca_181624-2682.jpg',
        categoria: 'sombra',
        stock: 15,
        fecha: "Jue, 1 Octubre 2020 04:53:09 PM",

    },
    {
        id: 3,
        nombre: 'Abuelito',
        precio: 30,
        descripcion: 'aut non nostrumaut non nostrumaut non nostrum',
        foto: 'https://image.freepik.com/foto-gratis/coleccion-cactus-plantas-suculentas-vasos-papel_128711-3193.jpg',
        categoria: 'sol',
        stock: 15,
        fecha: "Jue, 1 Octubre 2020 04:53:09 PM",

    },
    {
        id: 4,
        nombre: 'Tulipan',
        precio: 80,
        descripcion: 'aut non nostrumaut non nostrumaut non nostrum',
        foto: 'https://images.pexels.com/photos/931166/pexels-photo-931166.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        categoria: 'sombra',
        stock: 15,
        fecha: "Jue, 1 Octubre 2020 04:53:09 PM",

    }
];